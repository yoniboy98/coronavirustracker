package io.JavaSpring.CoronaVirus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;

@SpringBootApplication
@EnableScheduling
public class CoronaVirusApplication {

	public static void main(String[] args) throws IOException, InterruptedException {
		SpringApplication.run(CoronaVirusApplication.class, args);



	}

}
