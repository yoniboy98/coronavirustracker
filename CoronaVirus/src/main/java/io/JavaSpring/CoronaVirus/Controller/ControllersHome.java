package io.JavaSpring.CoronaVirus.Controller;

import io.JavaSpring.CoronaVirus.Modules.LocationStats;
import io.JavaSpring.CoronaVirus.Service.CoronaDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class ControllersHome {
    @Autowired
    CoronaDataService coronaDataService;

    @GetMapping("/")
    public String home(Model model) {
        List<LocationStats> allstats = coronaDataService.getAllStats();
        int totalCases = allstats.stream().mapToInt(LocationStats::getLatestTotalCases).sum();
        model.addAttribute("locationStats",allstats);
        model.addAttribute("totalReportedCases",totalCases);
        return "home";
    }
}
